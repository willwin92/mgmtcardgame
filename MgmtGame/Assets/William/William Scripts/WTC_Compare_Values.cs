﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WTC_Compare_Values : MonoBehaviour {
    float highestValue = 0;
    public GameObject[] cards;
    public Text higherScore;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void comparevalues()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i].GetComponent<WTC_Card_Values>().value > highestValue)
            {
                highestValue = cards[i].GetComponent<WTC_Card_Values>().value;
            }
        }
        higherScore.text = highestValue + " is worth the most.";
    }
}
