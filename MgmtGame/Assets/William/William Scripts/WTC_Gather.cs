﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WTC_Gather : MonoBehaviour {
    public Vector3 GatherPos;
    public GameObject ThisCard;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.G))
        {
            ThisCard.transform.position = GatherPos;
        }
	}
}
