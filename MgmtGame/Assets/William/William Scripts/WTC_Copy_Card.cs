﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WTC_Copy_Card : MonoBehaviour {

    public GameObject ThisCard;
    public JWPMouseManager TheMouse;
    public float ImHere;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (TheMouse.select==true)
        {
            CopyCard();            
        }
	}
    
    void CopyCard()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Instantiate(TheMouse.selectedObject);
            new Vector3(ImHere, 1, ImHere);
        }
    }
}
