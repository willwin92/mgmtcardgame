﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JWPRotateCard : MonoBehaviour {

	public JWPMouseManager target;

    [SerializeField]
    Material selectedMat;

    Material originalMat;

    private void Start()
    {
        originalMat = GetComponent<Renderer>().material;
    }
    void Update()
    {
        if (target.select == true)
        {
            CardRotate();
        }
    }

    void CardRotate()
    {
        if ( Input.GetKey(KeyCode.R))
        {
            Debug.Log("Im selected");
            target.selectedObject.transform.Rotate(0, Time.deltaTime * 100, 0);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            target.selectedObject.transform.Rotate(180, 0, 0);
        }
    }

    //All of this should be on the individual card. That way if you want one to be different, you don't need edge case code.
    void SelectedMat()
    {
        GetComponent<Renderer>().material = selectedMat;
    }
    void Deselect()
    {
        GetComponent<Renderer>().material = originalMat;
    }
}
