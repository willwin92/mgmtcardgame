﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JWPMouseManager : MonoBehaviour {
    //Once your done with a script, clean up the things you arent using.
    public GameObject selectedObject;
    public bool select;
    public Material originalMat;
    public Material selectedMat;


	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;

        if(Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.collider.gameObject.tag == "Card")
            {
                GameObject hitObject = hitInfo.transform.root.gameObject;
                hitObject.SendMessage("SelectedMat");
                SelectObject(hitObject);
            }
        }
        else
        {                     
                ClearSelection();           
        }
	}

    void SelectObject(GameObject obj)
    {
        
        if (selectedObject != null)
        {
         if (obj == selectedObject)

            return; 
            ClearSelection();
           
        }
        selectedObject = obj;
        //obj.GetComponent<Renderer>().material = selectedMat;
        select = true;   
    }

    void ClearSelection()
    {
        if (selectedObject == null)
            return;
        select = false;
        selectedObject.SendMessage("Deselect");//Any card script you have thats different can have this function. 
        //selectedObject.GetComponent<Renderer>().material = originalMat;   
        selectedObject = null;
    }
}
